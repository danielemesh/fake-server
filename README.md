# Fake Server #
## Installation
* clone the repo
* Run ```npm install```
* Add **FAKE_SERVER_PORT=4005** as an environment variable to your OS.  
* Run ```npm start```
* On the **server**, run ```npm run prod```

## Usage
* open your browser [localhost:4005/app/<APP_ID>/?var=value](http://localhost:4005/app/<APP_ID>/?var=value)
* Replace **<APP_ID>** with the application id.
* Edit the query string as you wish.
* The formatted response will be displayed in the browser.

## Loggly
Each request to the server will be submitted to Loggly.  
Go to [dauup.loggly.com](https://dauup.loggly.com) to see the logs.