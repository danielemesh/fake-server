"use strict";

const express = require("express");
const winston = require("winston");
require("winston-loggly");

let port        = process.env.FAKE_SERVER_PORT || 4005;
let env         = process.env.NODE_ENV || "dev";
let logglyToken = process.env.LOGGLY_TOKEN || "";

const logglyOptions = {
	inputToken: logglyToken,
	subdomain : "dauup",
	tags      : ["fake-server", `environment-${env}`],
	json      : true
};

winston.add(winston.transports.Loggly, logglyOptions);

let app = express();
app.set("view engine", "pug");

app.get("/app/:app_id/", (req, res) => {
	const query = req.query;
	const args  = {
		app_id      : req.params.app_id,
		original_url: req.originalUrl,
		query       : query
	};

	winston.info("Fake Server", args);

	res.render("index", Object.assign({}, args, {
		query: JSON.stringify(query)
	}));
});

app.listen(port, () => {
	console.log(`app listening on port ${port}`);
});